
import { AntdSkCheckbox }  from '../../sk-checkbox-antd/src/antd-sk-checkbox.js';

export class Antd4SkCheckbox extends AntdSkCheckbox {

    get prefix() {
        return 'antd4';
    }

}
