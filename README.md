# Skinny Widgets Checkbox for Antd4 Theme


checkbox element

```
npm i sk-checkbox sk-checkbox-antd4 --save
```

then add the following to your html

```html
<sk-config
    theme="antd"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd4"
></sk-config>
<sk-checkbox id="myCheckbox">Checkbox</sk-checkbox>
<script type="module">
    import { SkCheckbox } from './node_modules/sk-checkbox/index.js';

    customElements.define('sk-checkbox', SkCheckbox);

    myCheckbox.addEventListener('change', (event) => {
        alert('Changed !');
    });
</script>
```
#### slots

**default (not specified)** - draws label for input

**label** - draws label for input

#### attributes

**value** - value syncronized with internal native element

**disabled** - disabled attribute syncronized with internal native element

#### template

id: SkCheckboxTpl